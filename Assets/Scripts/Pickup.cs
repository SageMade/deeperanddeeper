using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string PickupSound;
    [FMODUnity.EventRef]
    public string DropSound;

    public float distanceFromPlayer;
    Player player;
    PickupCount pickupCount;
    public bool isPickedup = false;
    [SerializeField] Rigidbody2D rb;
    [SerializeField] Collider2D collider2D;

    public UnityEngine.Events.UnityEvent OnPickup;
    public UnityEngine.Events.UnityEvent OnDrop;

    private bool canDropBox = false;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<Player>();
        pickupCount = GameObject.Find("Player").GetComponent<PickupCount>();
        Physics2D.IgnoreLayerCollision(6, 10);
    }

    // Update is called once per frame
    void Update()
    {
        distanceFromPlayer = Vector2.Distance(player.transform.position, transform.position);
        bool interactedThisFrame = false;

        if (!interactedThisFrame && distanceFromPlayer <= 1.25f && Input.GetButtonDown("Fire1") && pickupCount.pickupTarget == null)
        {
            isPickedup = true;
            rb.isKinematic = true;
            //collider2D.enabled = false;
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            //rb.simulated = false;
            transform.eulerAngles = new Vector3(0, 0, 0);
            interactedThisFrame = true;
            AudioController.Instance.FireSound(PickupSound, transform.position);
            OnPickup?.Invoke();
        }


        if (isPickedup)
        {
            PickupBox();

            if(!interactedThisFrame && Input.GetButtonDown("Fire1") && player.canDropItem)
            {
                DropBox();
                OnDrop?.Invoke();
                interactedThisFrame = true;
            }
        }
    }

    void PickupBox()
    {
        transform.position = new Vector3(player.transform.position.x, player.transform.position.y + 0.75f, player.transform.position.z);
        pickupCount.pickupCounter = 1;
        gameObject.layer = 10;
        pickupCount.pickupTarget = gameObject;
    }

    void DropBox() 
    {
        AudioController.Instance.FireSound(DropSound, transform.position);
        rb.velocity = new Vector2(0,0); 
        isPickedup = false;
        rb.isKinematic = true;
        rb.bodyType = RigidbodyType2D.Dynamic;
        collider2D.enabled = true;
        rb.constraints = RigidbodyConstraints2D.None;
        transform.position = new Vector3(player.isFacingRight ? player.transform.position.x + 1 : player.transform.position.x - 1, player.transform.position.y, player.transform.position.z);
        transform.eulerAngles = new Vector3(0, 0, 0);
        pickupCount.pickupCounter = 0;
        pickupCount.pickupTarget = null;
        gameObject.layer = 7;
    }
}
