using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bully : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string DeathSound;

    public Transform box;
    [SerializeField] private Transform boxSpawn;
    public float distance;
    public bool playerRight;

    public GameObject HealthBarPanel;
    public GameObject ImagePrefab;
    public GameObject HeartBurst;
    public TransitionTextAsset EndText;
    public int        Health = 5;

    private Player player;

    float attackTimer;
    public float attackCD;

    private int _startHealth;
    private float _startCD;

    // Start is called before the first frame update
    void Start()
    {
        _startHealth = Health;
        _startCD = attackCD;
        player = GameObject.Find("Player").GetComponent<Player>();
        attackTimer = attackCD;

        for(int ix = 0; ix < Health; ix++) {
            GameObject.Instantiate(ImagePrefab, HealthBarPanel.transform);
        }
    }

    public void Damage() {
        Health--;
        if (HealthBarPanel.transform.childCount > 0) {
            Destroy(HealthBarPanel.transform.GetChild(0).gameObject);
        }
        SpawnHearts();
        if (Health <= 0) {
            StartCoroutine(FinishGame());
        }

        attackCD = _startCD * (1.0f / (Health / (float)_startHealth));
    }

    IEnumerator FinishGame() {
        SpriteRenderer renderer = GetComponentInChildren<SpriteRenderer>();
        player.enabled = false;
        attackTimer = float.MaxValue;
        Color col = renderer.color;
        FindObjectOfType<FollowPlayer>().player = transform;
        FindObjectOfType<FollowPlayer>().altFocus = null;
        for (float i = 3.0f; i >= 0.01f; i /= 1.5f) {
            SpawnHearts();
            col.a = 0.3f;
            renderer.color = col;
            yield return new WaitForSeconds(0.4f);
            col.a = 1.0f;
            renderer.color = col;
            yield return new WaitForSeconds(i);
        }
        col.a = 0.0f;
        renderer.color = col;
        AudioController.Instance.FireSound(DeathSound);
        SpawnHearts();
        SpawnHearts();
        SpawnHearts();
        yield return new WaitForSeconds(3.0f);
        GameStateManager.Instance.LevelCompletion[Minigame.Bully] = true;
        GameStateManager.Instance.TargetScene = "Scenes/HubWorld";
        GameStateManager.Instance.TransitionText = EndText;
        SceneTransitionManager.Instance.FadeToScene("Scenes/TransitionScene", false);
    }

    void SpawnHearts() {
        GameObject hearts = GameObject.Instantiate(HeartBurst);
        hearts.transform.position = transform.position;
        hearts.GetComponent<ParticleSystem>().Play();
    }

    // Update is called once per frame
    void Update()
    {
        distance = transform.position.x - player.transform.position.x;
        attackTimer -= Time.deltaTime;

        if (distance > 0)
            playerRight = false;
        else
            playerRight = true;

        Attack();
    }

    void Attack()
    {
        if (attackTimer <= 0)
        {
            Instantiate(box, boxSpawn.transform.position, Quaternion.identity);
            attackTimer = attackCD;
        }
        
    }
}
