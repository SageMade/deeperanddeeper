using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedWorldRotation : MonoBehaviour
{
    Quaternion _worldRot;

    // Start is called before the first frame update
    void Start() {
        _worldRot = transform.rotation;    
    }

    // Update is called once per frame
    void Update() {
        transform.rotation = _worldRot;
    }
}
