using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupCount : MonoBehaviour
{
    public int pickupCounter = 0;
    public GameObject pickupTarget = null;

    // Start is called before the first frame update
    void Start()
    {
        pickupCounter = 0;
    }

    public void ResetCounter()
    {
        pickupCounter = 0;
    }
}
