using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionSafeZone : MonoBehaviour
{

    public bool isSafe = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("SafeZone"))
        {
            isSafe = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("SafeZone"))
        {
            isSafe = false;
        }
    }
}
