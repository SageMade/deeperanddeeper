using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeamController : MonoBehaviour
{
    public ParticleSystem Hearts;
    public float          TargetExplosionHeight = 10.0f;
    public float          Speed = 2.0f;
    public float          CamZoomSize = 30.0f;

    public bool PlayOnAwake = false;
    public bool CanFire {
        get;
        private set;
    }

    private Vector3 _startingPos;
    private FollowPlayer _cam;

    public void Awake() {
        _startingPos = transform.position;
        _cam = Camera.main.gameObject.GetComponent<FollowPlayer>();
        CanFire = true; // I think you'll find this battle station is quite operational >:)
        if (PlayOnAwake) {
            Fire();
        }
    }

    public void Fire() {
        if (CanFire) {
            StartCoroutine(PlayFire());
        }
    }

    private IEnumerator PlayFire() {
        transform.position = _startingPos;
        CanFire = false;
        Hearts.Play();
        _cam.altFocus = transform;
        while (transform.position.y < _startingPos.y + TargetExplosionHeight) {
            transform.position += new Vector3(0.0f, Time.deltaTime * Speed);
            yield return null;
        }
        Hearts.Stop();
        FindObjectOfType<Bully>()?.Damage();
        yield return new WaitForSeconds(1.0f);
        while (transform.position.y > _startingPos.y) {
            transform.position -= new Vector3(0.0f, Time.deltaTime * Speed);
            yield return null;
        }
        CanFire = true;
        _cam.altFocus = null;
    }

    // Start is called before the first frame update
#if UNITY_EDITOR
    private void OnDrawGizmosSelected() {
        Vector3 pos = transform.position;
        UnityEditor.Handles.color = UnityEditor.Handles.yAxisColor;
        Vector3 newPos = UnityEditor.Handles.Slider(pos + new Vector3(0, TargetExplosionHeight), Vector3.up);
        TargetExplosionHeight = newPos.y - pos.y;
    }
#endif
}
