using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositiveDropoffController : MonoBehaviour
{
    public BeamController Beam;

    private PickupCount _pickup;

    // Start is called before the first frame update
    void Start()
    {
        _pickup = FindObjectOfType<PickupCount>();
    }

    // Update is called once per frame
    private void OnTriggerStay2D(Collider2D collision) {
        if (collision.gameObject.layer == 10) { //"PlayerHold"
            BoxPhrases b = collision.gameObject.GetComponent<BoxPhrases>();
            if (b != null && b.isPositive) {
                if (Beam.CanFire) {
                    Destroy(collision.gameObject);
                    Beam.Fire();
                    _pickup.pickupTarget = null;
                    _pickup.pickupCounter = 0;
                }
            }
        }
    }
}
