using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BullyGameManager : MonoBehaviour 
{
    [Header("Level Specific")]
    [SerializeField] private TransitionTextAsset OnDeathText;

    // Start is called before the first frame update
    void Start()
    {
        GameStateManager.Instance.OnPlayerHealthChanged.AddListener(OnPlayerHealthChanged);   
    }

    private void OnDestroy() {
        GameStateManager.Instance.OnPlayerHealthChanged.RemoveListener(OnPlayerHealthChanged);
    }

    private void OnPlayerHealthChanged(float health, float max) {
        if (health <= 0.0f) {
            GameStateManager.Instance.Player.enabled = false;
            GameStateManager.Instance.TargetScene = "Scenes/Bully";
            GameStateManager.Instance.TransitionText = OnDeathText;
            SceneTransitionManager.Instance.FadeToScene("Scenes/TransitionScene");
        }
    }
}
