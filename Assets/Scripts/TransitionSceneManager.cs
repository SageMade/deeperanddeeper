using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TransitionSceneManager : MonoBehaviour
{
    public float TimeBetweenText = 2.0f;
    public float TextFadeTime = 1.0f;
    public Transform Panel;
    public TransitionTextAsset TextAsset;
    public TextMeshProUGUI ContinueText;
    public GameObject TextPrefab;

    protected TextMeshProUGUI[] _lines = new TextMeshProUGUI[0];
    protected Coroutine _currentRoutine;
    protected bool _canContinue = false;
    protected TextMeshProUGUI _currentFading;
    protected int _currentFadeIndex;
    protected string _targetScene;

    // Start is called before the first frame update
    void Start(){
        _targetScene = GameStateManager.Instance.TargetScene;
        if (string.IsNullOrWhiteSpace(_targetScene)) {
            _targetScene = "Scenes/MainMenu";
        }

        if (TextAsset == null) {
            TextAsset = GameStateManager.Instance.TransitionText;
        }

        if (TextAsset != null && TextAsset.Lines.Length > 0) {
            _lines = new TextMeshProUGUI[TextAsset.Lines.Length];
            for (int ix = 0; ix < TextAsset.Lines.Length; ix++) {
                GameObject instance = GameObject.Instantiate(TextPrefab, Panel);
                instance.SetActive(true);
                TextMeshProUGUI text = instance.GetComponent<TextMeshProUGUI>();
                text?.SetText(TextAsset.Lines[ix]);
                text.horizontalAlignment = ix % 2 == 0 ? HorizontalAlignmentOptions.Left : HorizontalAlignmentOptions.Right;
                Color c = text.color;
                c.a = 0.0f;
                text.color = c;
                _lines[ix] = text;
            }
        }
        _currentRoutine = StartCoroutine(FadeTextLine());
        _currentFading = _lines.Length > 0 ? _lines[0] : ContinueText;
        _currentFadeIndex = 0;
    }

    IEnumerator FadeTextLine() {
        if (_currentFadeIndex < _lines.Length) {
            _currentFading = _lines[_currentFadeIndex];
        } else {
            _currentFading = ContinueText;
        }

        Color c = _currentFading.color;
        c.a = 0.0f;
        _currentFading.color = c;
        while (c.a < 1.0f) {
            c.a += Time.deltaTime / TextFadeTime;
            _currentFading.color = c;
            yield return null;
        }
        if (_currentFadeIndex < _lines.Length) {
            yield return new WaitForSeconds(TimeBetweenText);
            _currentFadeIndex++;
            _currentRoutine = StartCoroutine(FadeTextLine());
        } else {
            _canContinue = true;
            _currentRoutine = null;
        }
    }

    void TransitionToNextScene() {
        SceneTransitionManager.Instance.FadeToScene(_targetScene);
    }

    // Update is called once per frame
    void Update() {
        if (Input.anyKeyDown) {
            if (_currentRoutine != null) {
                StopCoroutine(_currentRoutine);
                Color c = _currentFading.color;
                c.a = 1.0f;
                _currentFading.color = c;
                _currentFadeIndex++;

                if (_currentFadeIndex > _lines.Length) {
                    _canContinue = true;
                    _currentRoutine = null;
                } else {
                    _currentRoutine = StartCoroutine(FadeTextLine());
                }
            } 
            if (_canContinue) {
                Debug.Log("Moving to next scene");
                TransitionToNextScene();
            }
        }   
    }
}
