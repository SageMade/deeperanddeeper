using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BreathTimer : MonoBehaviour
{
    public TextMeshProUGUI timerText;
    public RectTransform bar;
    Breathing breathing;
    Player player;
    CollisionSafeZone safe;

    Vector2 _anchorMax;

    // Start is called before the first frame update
    void Start()
    {
        breathing = FindObjectOfType<Breathing>();
        player = GameObject.Find("Player").GetComponent<Player>();
        safe = GameObject.Find("Player").GetComponent<CollisionSafeZone>();
        timerText.text = breathing.timer.ToString();
        _anchorMax = bar.anchorMax;

    }

    // Update is called once per frame
    void Update()
    {
        _anchorMax.x = breathing.timerPercent;
        bar.anchorMax = _anchorMax;
        timerText.text = breathing.timer.ToString("F0");
        transform.position = new Vector3(player.transform.position.x, player.transform.position.y + 1f, player.transform.position.z);
        Debug.Log(breathing.timer);

        if(safe.isSafe || breathing.timer < 0)
            timerText.enabled = false;
        else
            timerText.enabled = true;
    }
}
