using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndSceneController : MonoBehaviour
{
    public TransitionTextAsset FinalText;
    [FMODUnity.EventRef]
    public string ChainSound;

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.CompareTag("Player")) {
            StartCoroutine(Cutscene());
        }
    }

    private IEnumerator Cutscene() {
        AudioController.Instance.FadeMusic(2.0f);
        yield return SceneTransitionManager.Instance.FadeToBlack(2.0f);
        AudioController.Instance.FireSound(ChainSound);
        yield return new WaitForSecondsRealtime(5.0f);
        GameStateManager.Instance.TargetScene = "Scenes/MainMenu";
        GameStateManager.Instance.TransitionText = FinalText;
        SceneTransitionManager.Instance.SwitchScene("Scenes/TransitionScene");
    }
}
