using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDictionary<TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>>, IEnumerable {
    private Dictionary<TKey, TValue> _dict = new Dictionary<TKey, TValue>();

    public TValue this[TKey key] {
        get {
            if (_dict.ContainsKey(key)) {
                return _dict[key];
            } else {
                return default(TValue);
            }
        }
        set {
            if (_dict.ContainsKey(key)) {
                bool invoke = EqualityComparer<TValue>.Default.Equals(_dict[key], value);
                _dict[key] = value;
                if (invoke)
                    OnValueChanged?.Invoke(key, value);
            } else {
                _dict.Add(key, value);
                OnValueChanged?.Invoke(key, value);
            }
        }
    }

    internal TValue GetOrDefault(TKey key, TValue def, bool insert = true) {
        if (_dict.ContainsKey(key)) {
            return _dict[key];
        } else {
            if (insert) {
                _dict.Add(key, def);
                OnValueChanged?.Invoke(key, def);
            }
            return def;
        }
    }

    public event Action<TKey, TValue> OnValueChanged;

    public int Count {
        get { return _dict.Count; }
    }

    public bool Add(TKey key, TValue value) {
        bool hasKey = _dict.ContainsKey(key);
        this[key] = value;
        return !hasKey;
    }

    public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() {
        return _dict.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator() {
        return _dict.GetEnumerator();
    }

    public void Clear() {
        _dict.Clear();
    }
}
