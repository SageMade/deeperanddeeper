using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform player;
    public Transform altFocus;

    public float minCamScale = 7.0f;
    public float maxFollowSpeed = 100.0f;
    public float maxZoomSpeed = 10.0f;

    private float _z;
    private Vector3 _camVelocity;

    // Start is called before the first frame update
    void Start()
    {
        _z = transform.position.z;
        if (player == null) {
            player = GameStateManager.Instance.Player.transform;
        }
    }

    // Update is called once per frame
    void FixedUpdate() {
        Camera cam = Camera.main;
        if (altFocus == null) {
            Vector3 pos = Vector3.SmoothDamp(transform.position, player.transform.position, ref _camVelocity, 0.1f, maxFollowSpeed, Time.fixedDeltaTime);
            pos.z = _z;
            transform.position = pos;
            if (!Mathf.Approximately(cam.orthographicSize, minCamScale)) {
                cam.orthographicSize = Mathf.MoveTowards(cam.orthographicSize, minCamScale, maxZoomSpeed * Time.deltaTime);
            }
        } else {
            Vector3 center = (altFocus.position + player.position) / 2.0f;
            Vector3 pos = Vector3.SmoothDamp(transform.position, center, ref _camVelocity, 0.1f, maxFollowSpeed, Time.fixedDeltaTime);
            pos.z = _z;
            transform.position = pos;
            float zoom = Mathf.Max(Mathf.Abs(altFocus.position.x - player.position.x), Mathf.Abs(altFocus.position.y - player.position.y) * cam.aspect);
            zoom = zoom < minCamScale ? minCamScale : zoom;
            cam.orthographicSize = Mathf.MoveTowards(cam.orthographicSize, zoom, maxZoomSpeed * Time.deltaTime);
        }
    }
}
