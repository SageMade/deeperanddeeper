using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DDOL : MonoBehaviour
{
    private static AutoDictionary<string, GameObject> _ddolInstances = new AutoDictionary<string, GameObject>();

    // Start is called before the first frame update
    void Awake()
    {
        if (_ddolInstances[gameObject.name] == null) {
            _ddolInstances[gameObject.name] = gameObject;
            DontDestroyOnLoad(gameObject);
        } else {
            Destroy(gameObject);
        }
    }
}
