using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BoxPhrases : MonoBehaviour
{
    public string[] badVibes = new string[] { "I Hate You", "Ugly", "No Friends", "You're Adopted", "You're a Joke", "Stupid", "Freak", "Fat", "Loner", "Unloved", "Useless", "Worthless", "Dumb", "Idiot", "Lame", "Nerd", "Loser" };
    public string[] goodVibes = new string[] {"You're Loved", "I'm You're Friend", "Smart", "Brave"};
    public TextMeshProUGUI text;
    [SerializeField] private int badPhraseRandom;
    [SerializeField] private int goodPhraseRandom;
    public bool isPositive = false;
    SwitchPositiveVibes switchPositiveVibes;
    PickupCount pickupCount;
    BossHealth bossHealth;

    // Start is called before the first frame update
    void Start()
    {
        badPhraseRandom = Random.Range(0, badVibes.Length);
        text.text = badVibes[badPhraseRandom];
        goodPhraseRandom = Random.Range(0, goodVibes.Length);
        switchPositiveVibes = GameObject.Find("Positive").GetComponent<SwitchPositiveVibes>();
        pickupCount = GameObject.Find("Player").GetComponent<PickupCount>();
        bossHealth = GameObject.Find("Bully").GetComponent<BossHealth>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("PositiveVibes") && !isPositive)
        {
            text.text = goodVibes[goodPhraseRandom];
            switchPositiveVibes.AddCounter();
            isPositive = true;
        }

        if (collision.gameObject.CompareTag("Deposit") && isPositive)
        {
            Destroy(gameObject);
            pickupCount.ResetCounter();
            bossHealth.TakeDamage();
        }
    }
}
