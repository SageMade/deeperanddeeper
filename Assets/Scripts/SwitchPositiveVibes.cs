using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchPositiveVibes : MonoBehaviour
{
    public GameObject positive1;
    public GameObject positive2;
    public int counter = 0;
    // Start is called before the first frame update
    void Start()
    {
        positive1 = this.gameObject.transform.GetChild(0).gameObject;
        positive2 = this.gameObject.transform.GetChild(1).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if(counter % 2 == 0)
        {
            positive1.SetActive(true);
            positive2.SetActive(false);
        }
        else
        {
            positive1.SetActive(false);
            positive2.SetActive(true);
        }
    }

    public void AddCounter()
    {
        counter++;
    }
}
