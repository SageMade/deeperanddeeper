using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PositionSerializer : MonoBehaviour
{
    public string Key;

    // Start is called before the first frame update
    void Awake() {
        string key = string.Format("{0}:{1}", SceneManager.GetActiveScene().name, string.IsNullOrWhiteSpace(Key) ? gameObject.name : Key);
        float z = transform.position.z;
        Vector2 pos = GameStateManager.Instance.SaveState.Vec2Metas.GetOrDefault(key, transform.position);
        transform.position = new Vector3(pos.x, pos.y, z);
    }

    private void OnDestroy() {
        string key = string.Format("{0}:{1}", SceneManager.GetActiveScene().name, string.IsNullOrWhiteSpace(Key) ? gameObject.name : Key);
        Vector2 pos = GameStateManager.Instance.SaveState.Vec2Metas[key] = transform.position;
    }
}
