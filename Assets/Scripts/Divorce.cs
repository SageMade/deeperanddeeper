using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Divorce : MonoBehaviour
{
    public bool levelComplete = false;
    public TextMeshProUGUI text;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.childCount <= 0)
        {
            levelComplete = true;
        }

        text.text = (10 - transform.childCount).ToString() + " / 10";
    }
}
