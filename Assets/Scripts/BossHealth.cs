using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHealth : MonoBehaviour
{
    public float bossHealth;
    private float maxHealth;
    [SerializeField] private BossHealthBar healthBar;
    // Start is called before the first frame update
    void Start()
    {
        maxHealth = bossHealth;
        healthBar = GameObject.Find("Boss Health").GetComponent<BossHealthBar>();
    }

    // Update is called once per frame
    void Update()
    {
        if(bossHealth <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void TakeDamage()
    {
        bossHealth--;
        healthBar.SetSize(bossHealth / maxHealth);
    }
}
