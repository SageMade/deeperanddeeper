using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public Vector3 point1;
    public Vector3 point2;
    public Vector3 point3;
    public Vector3 point4;

    [SerializeField] private float speed;

    public bool moveTo1;
    public bool moveTo2;
    public bool moveTo3;
    public bool moveTo4;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float step = speed * Time.deltaTime;

        if (moveTo1)
        {
            transform.position = Vector2.MoveTowards(transform.position, point1, step);
        }
        else if (moveTo2)
        {
            transform.position = Vector2.MoveTowards(transform.position, point2, step);
        }
        else if (moveTo3)
        {
            transform.position = Vector2.MoveTowards(transform.position, point3, step);
        }
        else if (moveTo4)
        {
            transform.position = Vector2.MoveTowards(transform.position, point4, step);
        }

        if(transform.position == point1)
        {
            moveTo1 = false;
            moveTo2 = true;
        }
        if (transform.position == point2)
        {
            moveTo2 = false;
            moveTo3 = true;
        }
        if (transform.position == point3)
        {
            moveTo3 = false;
            moveTo4 = true;
        }
        if (transform.position == point4)
        {
            moveTo4 = false;
            moveTo1 = true;
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.transform.SetParent(transform);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.transform.SetParent(null);
        }
    }
}

