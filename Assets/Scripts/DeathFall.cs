using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathFall : MonoBehaviour {
    [FMODUnity.EventRef]
    public string DeathSound;

    public float DeathLine = -100.0f;

    // Start is called before the first frame update
    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(new Vector3(-1000.0f, DeathLine), new Vector3(1000.0f, DeathLine));
    }

    private void Update() {
        if (GameStateManager.Instance.Player.transform.position.y < DeathLine) {
            GameStateManager.Instance.PlayerHealth = 0.0f;
            AudioController.Instance.FireSound(DeathSound);
            FollowPlayer follow = FindObjectOfType<FollowPlayer>();
            if (follow != null) { follow.enabled = false; }
        }
    }
}
