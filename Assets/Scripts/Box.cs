using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    public bool isTossed = true;
    public Vector2 force;
    private Bully bully;
    [SerializeField] private Vector2 tossForce;
    public bool pickedUp = false;
    public float boxLifetime = 5.0f;
    public float FadeTime = 2.0f;

    public bool IsGoodVibes = false;

    private bool _isDeleting = false;
    private float _currentLife = 0.0f;
    private Coroutine _fadeCoroutine;

    [SerializeField] private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        bully = GameObject.Find("Bully").GetComponent<Bully>();
        _currentLife = boxLifetime;
    }

    // Update is called once per frame
    void Update()
    {
        tossForce = new Vector2(Mathf.Abs(bully.distance / 2), Mathf.Abs(bully.distance / 2));

        if (pickedUp) {
            _currentLife = boxLifetime;
            if (_fadeCoroutine != null) {
                StopCoroutine(_fadeCoroutine);
                _fadeCoroutine = null;
                CanvasGroup renderer = GetComponentInChildren<CanvasGroup>();
                renderer.alpha = 1.0f;
            }
        } else {
            _currentLife -= Time.deltaTime;
        }

        if (isTossed)
        {
            rb.AddForce(bully.playerRight ? tossForce : new Vector2(-tossForce.x, tossForce.y), ForceMode2D.Impulse);
            rb.AddTorque(force.x, ForceMode2D.Impulse);
            isTossed = false;
        }

        if (_currentLife <= 0.0f && _fadeCoroutine == null) {
            _fadeCoroutine = StartCoroutine(FadeOut());
        }
    }

    public void Pickup() { pickedUp = true; }
    public void Drop() { pickedUp = false; }

    IEnumerator FadeOut() {
        CanvasGroup renderer = GetComponentInChildren<CanvasGroup>();
        while (renderer.alpha > 0.0f) {
            renderer.alpha -= Time.deltaTime / FadeTime;
            yield return null;
        }
        Destroy(gameObject);
    }
}
