using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameStateManager : MonoBehaviour
{
    [SceneSelector]
    public string HubWorldScene = "Scenes/HubWorld";
    [FMODUnity.EventRef]
    public string ChainBreak;

    public static GameStateManager Instance {
        get;
        protected set;
    }

    public string TargetScene {
        get;
        set;
    }

    public Vector3? HubTeleporterPos {
        get { return _saveState.SpawnLocation; }
        set { _saveState.SpawnLocation = value; }
    }
      

    public AutoDictionary<Minigame, bool> LevelCompletion {
        get { return _saveState.LevelCompletion; }
    }

    protected float _playerHealth = 100.0f;
    protected float _maxPlayerHealth = 100.0f;
    public float PlayerHealth {
        get { return _playerHealth; }
        set {
            float prev = _playerHealth;
            _playerHealth = Mathf.Clamp(value, 0.0f, _maxPlayerHealth);
            if (_playerHealth != prev) {
                OnPlayerHealthChanged?.Invoke(_playerHealth, _maxPlayerHealth);
            }
        }
    }
    public float MaxPlayerHealth {
        get { return _maxPlayerHealth; }
        set {
            float prev = _maxPlayerHealth;
            _maxPlayerHealth = value;
            if (_maxPlayerHealth != prev) {
                _playerHealth = Mathf.Clamp(_playerHealth, 0.0f, _maxPlayerHealth);
                OnPlayerHealthChanged?.Invoke(_playerHealth, _maxPlayerHealth);
            }
        }
    }
    public UnityEvent<float, float> OnPlayerHealthChanged;

    private int _saveSlot = 0;
    public int SaveSlot {
        get { return _saveSlot; }
        set {
            _saveSlot = value;
            PlayerPrefs.SetInt("save_slot", value);
            PlayerPrefs.Save();
            LoadScene();
        }
    }

    public Player Player {
        get;
        set;
    }
    public TransitionTextAsset TransitionText { get; internal set; }

    SaveGameState _saveState = new SaveGameState();
    public SaveGameState SaveState {
        get { return _saveState; }
    }

    // Start is called before the first frame update
    void Awake() {
        if (Instance == null) {
            Instance = this;
            SaveSlot = PlayerPrefs.GetInt("save_slot", -1);
            SceneManager.sceneUnloaded += SceneManager_sceneUnloaded;
            LevelCompletion.OnValueChanged += (k, v) => { 
                if (v) {
                    AudioController.Instance.FireSound(ChainBreak);
                }
                SaveScene(); 
            };
        } else {
            Destroy(this);
        }
    }

    public void SaveScene() {
        MemoryStream stream = new MemoryStream();
        _saveState.Serialize(stream);
        string save = Convert.ToBase64String(stream.ToArray());
        PlayerPrefs.SetString(string.Format("saves_{0}", SaveSlot), save);
        PlayerPrefs.Save();
        Debug.LogFormat("Saved game state: {0}", save);
    }

    private void LoadScene() {
        if (_saveSlot != -1 && PlayerPrefs.HasKey(string.Format("saves_{0}", _saveSlot))) {
            string save = PlayerPrefs.GetString(string.Format("saves_{0}", _saveSlot));
            Debug.LogFormat("Attempting to decode save: {0}", save);
            if (!string.IsNullOrWhiteSpace(save)) {
                byte[] data = Convert.FromBase64String(save);
                MemoryStream stream = new MemoryStream(data);
                _saveState.DeSerialize(stream);
                foreach(var kvp in _saveState.LevelCompletion) {
                    Debug.LogFormat("{0}: {1}", kvp.Key, kvp.Value);
                }
            }
        }
    }

    private void SceneManager_sceneUnloaded(Scene arg0) {
        _saveState.TimePlayed += Time.timeSinceLevelLoad;
        SaveScene();

    }

    private void Start() {
        OnPlayerHealthChanged?.Invoke(_playerHealth, _maxPlayerHealth);
    }

    public void SetPlayer(Player player) {
        Player = player;
    }

    // Update is called once per frame
    void Update()
    {
        if (Application.isEditor) {
            if (Input.GetKeyUp(KeyCode.R)) {
                PlayerHealth = _maxPlayerHealth;
            }
            if (Input.GetKeyUp(KeyCode.F1)) {
                PlayerPrefs.DeleteAll();
                PlayerPrefs.Save();
            }
        }
    }
}
