using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class GameSlotButton : MonoBehaviour
{
    public int Slot;
    public bool IsMakingNewGame;

    private SaveGameState _state = new SaveGameState();
    private Rect windowRect = new Rect((Screen.width - 200) / 2, (Screen.height - 300) / 2, 200, 300);
    // Only show it if needed.
    private bool showPrompt = false;
    private string alertMessage;
    private string key;
    private Action callback;

    void OnGUI() {
        if (showPrompt)
            windowRect = GUI.Window(0, windowRect, DialogWindow, alertMessage);
    }

    // This is the actual window.
    void DialogWindow(int windowID) {
        float y = 20;
        GUI.Label(new Rect(5, y, windowRect.width, 20), alertMessage);

        if (GUI.Button(new Rect(5, y, windowRect.width - 10, 20), "Yes")) {
            showPrompt = false;
            callback?.Invoke();
        }

        if (GUI.Button(new Rect(5, y, windowRect.width - 10, 20), "No")) {
            showPrompt = false;
        }
    }


    // Start is called before the first frame update
    void OnEnable()
    {
        Button b = GetComponent<Button>();
        b.interactable = true;
        TMPro.TextMeshProUGUI text = GetComponentInChildren<TMPro.TextMeshProUGUI>();
        b.onClick.AddListener(OnClicked);
        key = string.Format("saves_{0}", Slot);
        if (PlayerPrefs.HasKey(key)) {
            string saveData = PlayerPrefs.GetString(key);
            MemoryStream stream = new MemoryStream(Convert.FromBase64String(saveData));
            _state.DeSerialize(stream);
            text.text = string.Format("Slot {0} - {1:N1} mins", Slot, _state.TimePlayed / 60.0f);
        } else {
            text.text = string.Format("Slot {0} - Empty", Slot);
            if (!IsMakingNewGame) {
                b.interactable = false;
            }
        }
    }

    private void OnClicked() {
        if (IsMakingNewGame) {
            if (_state.TimePlayed > 0.0f) {
                showPrompt = true;
                alertMessage = "Do you want to overwrite this save?";
                callback = _NewGame;
            } else {
                _Load();
            }
        } else {
            _Load();
        }
    }

    private void _NewGame() {
        _state = new SaveGameState();
        MemoryStream stream = new MemoryStream();
        _state.Serialize(stream);
        PlayerPrefs.SetString(key, Convert.ToBase64String(stream.ToArray()));
        PlayerPrefs.Save();
        _Load();
    }

    private void _Load() {
        GameStateManager.Instance.SaveSlot = Slot;
        SceneTransitionManager.Instance.FadeToScene("Scenes/HubWorld");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
