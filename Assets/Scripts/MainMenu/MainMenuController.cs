using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Linq;

public class MainMenuController : MonoBehaviour
{
    public Transform SlotsPanel;
    public Transform MainPanel;
    public Transform OptionsPanel;

    public Button ContinueButton;
    public TMPro.TextMeshProUGUI SaveGameText;

    private Transform _activePanel;

    EventSystem _es;

    private void Awake() {
        _es = FindObjectOfType<EventSystem>();
        _activePanel = MainPanel;

        int slot = PlayerPrefs.GetInt("save_slot", -1);
        if (slot == -1) {
            ContinueButton.interactable = false;
            _es.SetSelectedGameObject(transform.GetChild(1).gameObject);
        }

        if (SaveGameText != null) {
            SaveGameText.text = string.Format("Selected save slot: {0}", slot == -1 ? "none" : slot.ToString());
        }
    }

    public void ShowNewGame() {
        GameSlotButton[] buttons = SlotsPanel.gameObject.GetComponentsInChildren<GameSlotButton>();
        foreach(var b in buttons) {
            b.IsMakingNewGame = true;
        }
        MainPanel.gameObject.SetActive(false);
        SlotsPanel.gameObject.SetActive(true);
        _activePanel = SlotsPanel;
    }
    public void ShowLoadGame() {
        GameSlotButton[] buttons = SlotsPanel.gameObject.GetComponentsInChildren<GameSlotButton>();
        foreach (var b in buttons) {
            b.IsMakingNewGame = false;
        }
        MainPanel.gameObject.SetActive(false);
        SlotsPanel.gameObject.SetActive(true);
        OptionsPanel.gameObject.SetActive(false);
        _activePanel = SlotsPanel;
    }
    public void ShowOptions() {
        SlotsPanel.gameObject.SetActive(false);
        MainPanel.gameObject.SetActive(false);
        OptionsPanel.gameObject.SetActive(true);
        _activePanel = OptionsPanel;
    }

    public void ToMain() {
        SlotsPanel.gameObject.SetActive(false);
        MainPanel.gameObject.SetActive(true);
        OptionsPanel.gameObject.SetActive(false);
        _activePanel = MainPanel;
    }

    private void Update() {
        if (Input.GetButtonDown("Cancel")) {
            ToMain();
        }

        if (_es.currentSelectedGameObject == null || !_es.currentSelectedGameObject.activeInHierarchy || !_es.currentSelectedGameObject.CompareTag("MenuButton")) {
            for(int ix = 0; ix < _activePanel.childCount; ix++) {
                Selectable b = _activePanel.GetChild(ix).GetComponent<Selectable>();
                if (b != null) {
                    if (b.interactable) {
                        b.Select();
                        break;
                    }
                }
            }
        }
    }

    public void Quit() {
        Application.Quit();
    }
}
