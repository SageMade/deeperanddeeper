using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("Audio")]
    [FMODUnity.EventRef] public string DeathSound;

    [Header("Game Mechanics")]
    [SerializeField] private float boxHarmThreshold = 5.0f;
    [SerializeField] private float boxDamageMultiplier = 2.0f;
    [SerializeField] private float boxDamageCooldown = 0.5f;

     [Header("Components")]
    [SerializeField] public Rigidbody2D rb;
    [SerializeField] private new Collider2D collider2D;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private Animator animator;

    [Header("Physics")]
    [SerializeField] private float maxSpeed;
    [SerializeField] private float linearDrag;
    [SerializeField] private float gravity;
    [SerializeField] private float fallMultiplier;

    [Header("Horizontal Movement")]
    [SerializeField] private float moveSpeed;
    [SerializeField] Vector2 direction;
    public bool isFacingRight = true;
    public bool isRunning = false;

    [Header("Vertical Movement")]
    [SerializeField] private float jumpSpeed;
    [SerializeField] private float jumpDelay;
    private float jumpTimer;
    [SerializeField] private int jumpCount;
    public int extraJumps;
    public float jumpPushForce;
    public float jumpForce;
    public float wallDelay;
    private float wallTimer;
    private bool  hasUsedWalljumpBonus = false;
    private bool  hasUsedWalljump = false;
    private float coyoteTime;
    [SerializeField] private float maxCoyoteTime = 0.5f;

    [Header("Collision")]
    public bool onGround = false;
    public float groundLength = 0.4f;
    public Vector3 colliderOffset;
    [SerializeField] private bool onWall = false;
    [SerializeField] private bool onWallRight = false;
    [SerializeField] private bool onWallLeft = false;
    public bool canDropItem = false;
    public float wallLength;

    private float damageCooldown = 0.0f;

    PickupCount _pickupTracker;

    // Start is called before the first frame update
    void Awake() {
        GameStateManager.Instance?.SetPlayer(this);
        _pickupTracker = GetComponent<PickupCount>();
    }

    private void OnDestroy() {
        if (GameStateManager.Instance?.Player == this) {
            GameStateManager.Instance.Player = null;
        }
    }

    // Update is called once per frame
    void Update()
    {
        onGround = Physics2D.Raycast(transform.position + colliderOffset, Vector2.down, groundLength, groundLayer) ||
            Physics2D.Raycast(transform.position - colliderOffset, Vector2.down, groundLength, groundLayer);

        onWallRight = Physics2D.Raycast(transform.position, Vector2.right, wallLength, groundLayer);
        onWallLeft = Physics2D.Raycast(transform.position, Vector2.left, wallLength, groundLayer);
        onWall = onWallRight || onWallLeft;

        Debug.DrawRay(transform.position + colliderOffset, Vector2.down * groundLength, Color.red);
        Debug.DrawRay(transform.position - colliderOffset, Vector2.down * groundLength, Color.red);

        if (!onGround)
        {
            coyoteTime -= Time.deltaTime;
        }

        if ((Input.GetButtonDown("Jump")))
        {
            jumpTimer = Time.time + jumpDelay;
        }

        if ((Input.GetButtonDown("Jump") && !onGround) && onWall)
        {
            wallTimer = Time.time + wallDelay;
        }

        if (Input.GetButtonDown("Jump") && !onGround && jumpCount > 0 && coyoteTime < 0) {
            if (!onWall || !((Input.GetAxis("Horizontal") < 0.0f && onWallLeft) || (Input.GetAxis("Horizontal") > 0.0f && onWallRight))) {
                Jump();
            }
        }

        if (onGround)
        {
            jumpCount = extraJumps;
            hasUsedWalljumpBonus = false;
            hasUsedWalljump = false;
            coyoteTime = maxCoyoteTime;
        }

        direction = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        if (damageCooldown > 0.0f) {
            damageCooldown -= Time.deltaTime;
        }

        // Placement logic
        {
            Vector2 dir = isFacingRight ? Vector2.right : Vector2.left;
            float width = _pickupTracker.pickupTarget != null ? _pickupTracker.pickupTarget.GetComponent<Collider2D>().bounds.size.x : 1.0f;
            width += 0.5f;
            bool wallTop = Physics2D.Raycast(transform.position + new Vector3(0.0f, 0.4f), dir, width, groundLayer);
            bool wallBottom = Physics2D.Raycast(transform.position - new Vector3(0.0f, 0.45f), dir, width, groundLayer);
            canDropItem = !(wallTop || wallBottom);

            Debug.DrawRay(transform.position + new Vector3(0.0f, 0.4f), dir * width, Color.green);
            Debug.DrawRay(transform.position - new Vector3(0.0f, 0.45f), dir * width, Color.green);
        }

        // Debug scene switching
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            GameStateManager.Instance.TargetScene = "Scenes/Bully";
            GameStateManager.Instance.TransitionText = UnityEditor.AssetDatabase.LoadAssetAtPath<TransitionTextAsset>("Assets/TextScrolls/Bully.asset");
            SceneTransitionManager.Instance.FadeToScene("Scenes/TransitionScene");
        }
        if (Input.GetKeyDown(KeyCode.Alpha2)) {
            GameStateManager.Instance.TargetScene = "Scenes/Death";
            GameStateManager.Instance.TransitionText = UnityEditor.AssetDatabase.LoadAssetAtPath<TransitionTextAsset>("Assets/TextScrolls/Death.asset");
            SceneTransitionManager.Instance.FadeToScene("Scenes/TransitionScene");
        }
        if (Input.GetKeyDown(KeyCode.Alpha3)) {
            GameStateManager.Instance.TargetScene = "Scenes/Breathing";
            GameStateManager.Instance.TransitionText = UnityEditor.AssetDatabase.LoadAssetAtPath<TransitionTextAsset>("Assets/TextScrolls/Flood.asset");
            SceneTransitionManager.Instance.FadeToScene("Scenes/TransitionScene");
        }
        if (Input.GetKeyDown(KeyCode.Alpha4)) {
            GameStateManager.Instance.TargetScene = "Scenes/Divorce";
            GameStateManager.Instance.TransitionText = UnityEditor.AssetDatabase.LoadAssetAtPath<TransitionTextAsset>("Assets/TextScrolls/Divorce.asset");
            SceneTransitionManager.Instance.FadeToScene("Scenes/TransitionScene");
        }
        if (Input.GetKeyDown(KeyCode.Alpha5)) {
            GameStateManager.Instance.TargetScene = "Scenes/Social";
            GameStateManager.Instance.TransitionText = UnityEditor.AssetDatabase.LoadAssetAtPath<TransitionTextAsset>("Assets/TextScrolls/Social.asset");
            SceneTransitionManager.Instance.FadeToScene("Scenes/TransitionScene");
        }

        if (Input.GetKeyDown(KeyCode.F2)) {
            Debug.Log("Marking all games as complete");
            GameStateManager.Instance.SaveState.LevelCompletion[Minigame.Bully] = true;
            GameStateManager.Instance.SaveState.LevelCompletion[Minigame.Death] = true;
            GameStateManager.Instance.SaveState.LevelCompletion[Minigame.Flood] = true;
            GameStateManager.Instance.SaveState.LevelCompletion[Minigame.Divorce] = true;
            GameStateManager.Instance.SaveState.LevelCompletion[Minigame.Social] = true;
        }
#endif
    }

    private void FixedUpdate()
    {
        if(gameObject.GetComponent<Rigidbody2D>() != null)
        {
            moveCharacter(direction.x);
            modifyPhysics();
        }

        if (jumpTimer > Time.time && coyoteTime > 0)
        {
            Jump();
            coyoteTime = 0;
            jumpCount = extraJumps; 
        }

        if (wallTimer > Time.time && onWall && !onGround)
        {
            if ((Input.GetAxis("Horizontal") < 0.0f && onWallLeft) || (Input.GetAxis("Horizontal") > 0.0f && onWallRight)) {
                WallJump();
            }
        }
    }

    void moveCharacter(float horizontal)
    {
        if (gameObject.GetComponent<Rigidbody2D>() != null)
            rb.AddForce(Vector2.right * horizontal * moveSpeed);

        if((horizontal > 0 && !isFacingRight) || (horizontal < 0 && isFacingRight))
        {
            Flip();
        }

        if (onWallRight)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            isFacingRight = true;
        }
        else if (onWallLeft)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
            isFacingRight = false;
        }

        if (Mathf.Abs(rb.velocity.x) > 0.2f)
        {
            isRunning = true;
        }
        else
        {
            isRunning = false;
        }

        if(Mathf.Abs(rb.velocity.x) > maxSpeed)
            rb.velocity = new Vector2(Mathf.Sign(rb.velocity.x) * maxSpeed, rb.velocity.y);

        animator.SetBool("isRunning", isRunning);
    }
    
    void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.AddForce(Vector2.up * jumpSpeed, ForceMode2D.Impulse);

        jumpTimer = 0;

        if (!onGround)
            jumpCount--;
    }

    void WallJump()
    {
        if (!hasUsedWalljump) {
            rb.velocity = new Vector2(jumpPushForce * (onWallRight ? -1 : 1), jumpForce);
            hasUsedWalljump = true;
        }
        if (!hasUsedWalljumpBonus) {
            jumpCount++;
            hasUsedWalljumpBonus = true;
        }
    }

    void modifyPhysics()
    {
        bool changingDirections = (direction.x > 0 && rb.velocity.x < 0) || (direction.x < 0 && rb.velocity.x > 0);

        if(onGround)
        {
            if (Mathf.Abs(direction.x) < 0.4f || changingDirections)
            {
                rb.drag = linearDrag;
            }
            else
            {
                rb.drag = 0f;
            }

            rb.gravityScale = 0;
        }
        else
        {
            rb.gravityScale = gravity;
            rb.drag = linearDrag * 0.15f;

            if (rb.velocity.y < 0)
            {
                rb.gravityScale = gravity * fallMultiplier;
            }
            else if (rb.velocity.y > 0 && !Input.GetButton("Jump"))
            {
                rb.gravityScale = gravity * (fallMultiplier / 2);
            }
        }
    }

    void Flip()
    {
        isFacingRight = !isFacingRight;
        transform.rotation = Quaternion.Euler(0, isFacingRight ? 0 : 180, 0);
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (damageCooldown <= 0.0f && collision.rigidbody != null)
        {
            Vector2 relativeSpeed = collision.rigidbody.velocity - rb.velocity;
            if (collision.gameObject.CompareTag("EnemyProjectiles") && collision.rigidbody.velocity.magnitude > boxHarmThreshold)
            {
                float damage = relativeSpeed.magnitude * boxDamageMultiplier;
                GameStateManager.Instance.PlayerHealth -= damage;
                damageCooldown = boxDamageCooldown;
            }
        }

        if (collision.gameObject.layer == 9){ //"Death"
            GameStateManager.Instance.PlayerHealth = 0;
            AudioController.Instance.FireSound(DeathSound);
        }
    }
}
