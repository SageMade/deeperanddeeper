using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Breathing : MonoBehaviour {
    public Transform mask;
    public Image fadeMask;

    public float MinEdgeFade = 0.8f;
    public float MaxOverlayOpacity = 0.5f;
    public float timer;
    public float RecoveryRate = 1.0f;

    public TransitionTextAsset DeathText;
    public TransitionTextAsset CompletionText;

    CollisionSafeZone player;

    public float maxTime {
        get;
        private set;
    }
    public float timerPercent {
        get { return timer / maxTime; }
    }
    private float dmgCD = 1f;
    private float damage = 2f;
    Vector3 _maskScale;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<CollisionSafeZone>();
        _maskScale = mask.localScale;
        maxTime = timer;

        GameStateManager.Instance.OnPlayerHealthChanged.AddListener(OnPlayerHealthChanged);
    }

    private void OnPlayerHealthChanged(float health, float maxHealth) {
        if (health <= 0.0f) {
            GameStateManager.Instance.Player.rb.simulated = false;
            GameStateManager.Instance.TargetScene = SceneManager.GetActiveScene().name;
            GameStateManager.Instance.TransitionText = DeathText;
            SceneTransitionManager.Instance.FadeToScene("Scenes/TransitionScene");
        }
    }

    private IEnumerator FinishGame() {
        GameStateManager.Instance.Player.enabled = false;
        yield return new WaitForSeconds(1.0f);
        GameStateManager.Instance.LevelCompletion[Minigame.Flood] = true;
        GameStateManager.Instance.TargetScene = "Scenes/HubWorld";
        GameStateManager.Instance.TransitionText = CompletionText;
        SceneTransitionManager.Instance.FadeToScene("Scenes/TransitionScene");
    }

    // Update is called once per frame
    void Update()
    {
        if (!player.isSafe)
        {
            timer -= Time.deltaTime;

            if(timer <= 0)
            {
                dmgCD -= Time.deltaTime;
                if(dmgCD <= 0)
                {
                    GameStateManager.Instance.PlayerHealth -= damage;
                    dmgCD = 1f;
                    damage += 2;
                }
                timer = 0;
            }
        }
        else
        {
            timer += Time.deltaTime * RecoveryRate;
            timer = Mathf.Min(timer, maxTime);
            damage = 2f;
        }
        mask.localScale = Vector3.Lerp(_maskScale * MinEdgeFade, _maskScale, timer / maxTime);
        Color c = fadeMask.color;
        c.a = Mathf.Lerp(MaxOverlayOpacity, 0.0f, timer / maxTime);
        fadeMask.color = c;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.CompareTag("Player")) {
            StartCoroutine(FinishGame());
        }
    }
}
