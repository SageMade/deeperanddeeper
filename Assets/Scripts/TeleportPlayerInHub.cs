using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportPlayerInHub : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake() {
        if (GameStateManager.Instance.HubTeleporterPos.HasValue) {
            GameStateManager.Instance.Player.transform.position = GameStateManager.Instance.HubTeleporterPos.Value;
        }    
    }
}
