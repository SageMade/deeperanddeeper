using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneTransitionManager : MonoBehaviour
{
    public static SceneTransitionManager Instance {
        get;
        private set;
    }

    public Color FadeColor   = Color.black;
    public float FadeOutTime = 1.0f;
    public float FadeInTime  = 1.0f;

    private Image _fadeImage;


    // Start is called before the first frame update
    void Awake() {
        if (Instance == null) {
            Instance = this;
            _fadeImage = GetComponentInChildren<Image>();
            _fadeImage.color = new Color(0.0f, 0.0f, 0.0f, 0.0f);

            SceneManager.sceneLoaded += (Scene, LoadSceneMode) => {
                StartCoroutine(FadeIn());
            };
        } else {
            Destroy(gameObject);
        }
    }

    public void FadeToScene(string sceneName) {
        StartCoroutine(FadeOut(sceneName, true));
    }

    public void FadeToScene(string sceneName, bool pauseTime) {
        StartCoroutine(FadeOut(sceneName, pauseTime));
    }

    public IEnumerator FadeToBlack(float time) {
        Color temp = FadeColor;
        temp.a = 0.0f;
        while (temp.a < 1.0f) {
            temp.a += Time.unscaledDeltaTime / time;
            _fadeImage.color = temp;
            _fadeImage.transform.parent.position = Camera.main.transform.position;
            yield return null;
        }
    }

    public void SwitchScene(string sceneName) {
        SceneManager.LoadScene(sceneName);
        GameStateManager.Instance.PlayerHealth = GameStateManager.Instance.MaxPlayerHealth;
        StartCoroutine(FadeIn());
    }

    private IEnumerator FadeOut(string sceneName, bool pauseTime = true) {
        if (pauseTime)
        {
            Time.timeScale = 0.0f;
        }
        yield return FadeToBlack(FadeOutTime);
        SwitchScene(sceneName);
    }

    private IEnumerator FadeIn() {
        Color temp = FadeColor;
        temp.a = 1.0f;
        while (temp.a > 0.0f) {
            temp.a -= Time.unscaledDeltaTime / FadeOutTime;
            _fadeImage.color = temp;
            yield return null;
        }
        _fadeImage.transform.parent.position = Camera.main.transform.position;
        Time.timeScale = 1.0f;
    }
}
