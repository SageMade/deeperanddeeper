using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;

public enum Minigame {
    Bully,
    Death,
    Flood,
    Divorce,
    Social,
    Unknown
}

public class SaveGameState
{
    public AutoDictionary<Minigame, bool> LevelCompletion = new AutoDictionary<Minigame, bool>();
    public AutoDictionary<string, Vector2> Vec2Metas = new AutoDictionary<string, Vector2>();
    public float TimePlayed = 0.0f;
    public Vector3? SpawnLocation;

    public void Serialize(Stream stream) {
        BinaryWriter writer = new BinaryWriter(stream);
        writer.Write(3u); // Level encoding version;
        writer.Write(TimePlayed);
        writer.Write(SpawnLocation.HasValue);
        if (SpawnLocation.HasValue) {
            writer.Write(SpawnLocation.Value.x);
            writer.Write(SpawnLocation.Value.y);
        }
        writer.Write(LevelCompletion.Count);
        foreach(var kvp in LevelCompletion) {
            writer.Write((int)kvp.Key);
            writer.Write(kvp.Value);
        }
        writer.Write(Vec2Metas.Count);
        foreach (var kvp in Vec2Metas) {
            writer.Write(kvp.Key);
            writer.Write(kvp.Value.x);
            writer.Write(kvp.Value.y);
        }
    }
    public void DeSerialize(Stream stream) {
        TimePlayed = 0.0f;
        SpawnLocation = null;
        LevelCompletion.Clear();
        BinaryReader reader = new BinaryReader(stream);
        uint version = reader.ReadUInt32();
        switch (version) {
            case 1u:
                DeSerialize_v1(reader);
                break;
            case 2u:
                DeSerialize_v2(reader);
                break;
            case 3u:
                DeSerialize_v3(reader);
                break;
            default:
                Debug.LogErrorFormat("Cannot load level version {0}, no decoder found", version);
                break;
        }
    }

    private void DeSerialize_v1(BinaryReader reader) {
        TimePlayed = reader.ReadSingle();
        bool hasSpawn = reader.ReadBoolean();
        if (hasSpawn) {
            float x = reader.ReadSingle();
            float y = reader.ReadSingle();
            SpawnLocation = new Vector3(x, y, 0.0f);
        }
        int completed = reader.ReadInt32();
        for (int ix = 0; ix < completed; ix++) {
            string name = reader.ReadString();
            bool value = reader.ReadBoolean();
            LevelCompletion[(Minigame)Enum.Parse(typeof(Minigame), name)] = value;
        }
    }
    private void DeSerialize_v2(BinaryReader reader) {
        TimePlayed = reader.ReadSingle();
        bool hasSpawn = reader.ReadBoolean();
        if (hasSpawn) {
            float x = reader.ReadSingle();
            float y = reader.ReadSingle();
            SpawnLocation = new Vector3(x, y, 0.0f);
        }
        int completed = reader.ReadInt32();
        for (int ix = 0; ix < completed; ix++) {
            string name = reader.ReadString();
            bool value = reader.ReadBoolean();
            LevelCompletion[(Minigame)Enum.Parse(typeof(Minigame), name)] = value;
        }
        int vec2MetaCount = reader.ReadInt32();
        for (int ix = 0; ix < vec2MetaCount; ix++) {
            string name = reader.ReadString();
            float x = reader.ReadSingle();
            float y = reader.ReadSingle();
            Vec2Metas[name] = new Vector2(x, y);
        }
    }
    private void DeSerialize_v3(BinaryReader reader) {
        TimePlayed = reader.ReadSingle();
        bool hasSpawn = reader.ReadBoolean();
        if (hasSpawn) {
            float x = reader.ReadSingle();
            float y = reader.ReadSingle();
            SpawnLocation = new Vector3(x, y, 0.0f);
        }
        int completed = reader.ReadInt32();
        for (int ix = 0; ix < completed; ix++) {
            int game = reader.ReadInt32();
            bool value = reader.ReadBoolean();
            LevelCompletion[(Minigame)game] = value;
        }
        int vec2MetaCount = reader.ReadInt32();
        for (int ix = 0; ix < vec2MetaCount; ix++) {
            string name = reader.ReadString();
            float x = reader.ReadSingle();
            float y = reader.ReadSingle();
            Vec2Metas[name] = new Vector2(x, y);
        }
    }

    private List<string> reverseStringFormat(string template, string str) {
        //Handels regex special characters.
        template = Regex.Replace(template, @"[\\\^\$\.\|\?\*\+\(\)]", m => "\\"
         + m.Value);

        string pattern = "^" + Regex.Replace(template, @"\{[0-9]+\}", "(.*?)") + "$";

        Regex r = new Regex(pattern);
        Match m = r.Match(str);

        List<string> ret = new List<string>();

        for (int i = 1; i < m.Groups.Count; i++) {
            ret.Add(m.Groups[i].Value);
        }

        return ret;
    }
}
