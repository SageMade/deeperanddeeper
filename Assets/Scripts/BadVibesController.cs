using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class BadVibesController : MonoBehaviour
{
    public readonly float FOLLOW_LOST_DIST = 10.0f;
    public readonly float FOLLOW_DIST = 5.0f;

    public enum State {
        Patrol,
        Follow
    }
    public Transform player;

    public State state = State.Patrol;
    public float rotationPower = 0.1f;
    public float AttackStrength = 25.0f;
    public float KickBack = 10.0f;
    public float AttackTime = 3.0f;
    public float PlayerBounceForce = 20.0f;
    public GameObject GorePrefab;
    public bool RandomPatrol = false;

    private float _attackTimer = 0.0f;
    private Rigidbody2D _body;

    // Start is called before the first frame update
    void Start()
    {
        _body = GetComponent<Rigidbody2D>();
        if (player == null) {
            player = GameStateManager.Instance.Player.transform;
        }
    }

    // Update is called once per frame
    void Update() {
        switch(state) {
            case State.Patrol: {
                    if (Vector3.Distance(player.position, transform.position) < FOLLOW_DIST) {
                        state = State.Follow;
                    } else if (RandomPatrol) {
                        _body.AddRelativeForce(new Vector2(1.0f, 0.0f));
                        _body.AddTorque(Mathf.Sin(Time.time) * rotationPower);
                    }
                } break;
            case State.Follow: {
                    if (Vector3.Distance(player.position, transform.position) > FOLLOW_LOST_DIST) {
                        state = State.Patrol;
                        _attackTimer = 0.0f;
                    } else {
                        Vector2 diff = player.position - transform.position;
                        diff.Normalize();
                        _body.AddForce(diff);
                    }
                }
                break;
        }
        _attackTimer += Time.deltaTime;
    }

    private void OnDrawGizmosSelected() {
        Gizmos.DrawWireSphere(transform.position, FOLLOW_DIST);
    }

    public void Die() {
        // TODO: Play death sound;
        if (GorePrefab != null) {
            GameObject gore = GameObject.Instantiate(GorePrefab);
            gore.transform.position = transform.position;
        }
        Destroy(gameObject);
    }

    
    private void OnCollisionStay2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Player")) {
            if (player.position.y > transform.position.y + 0.1f) {
                Die();
                player.GetComponent<Rigidbody2D>()?.AddForce(new Vector2(0.0f, PlayerBounceForce), ForceMode2D.Impulse);
            }
            else if (_attackTimer > AttackTime) {
                Vector2 diff = transform.position - player.position;
                GameStateManager.Instance.PlayerHealth -= AttackStrength;
                _body.AddForce(diff * KickBack, ForceMode2D.Impulse);
                _attackTimer = 0.0f;
                Debug.Log("oof");
            }
        }
    }
}
