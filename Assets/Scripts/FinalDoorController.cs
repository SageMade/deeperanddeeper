using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalDoorController : MonoBehaviour
{
    [FMODUnity.EventRefAttribute]
    public string OpenSound;

    private void Start() {

        if (GetIsOpen()) {
            ParticleSystem sys = GetComponentInChildren<ParticleSystem>();
            if (sys != null) {
                sys.Stop();
            }
        }
    }

    // Start is called before the first frame update
    private void Update() {
        if (Input.GetButtonDown("Fire1") && Vector3.Distance(transform.position, GameStateManager.Instance.Player.transform.position) < 1.0f) {
            bool isOpen = GetIsOpen();
            //
            //isOpen = true;

            if (isOpen) {
                Destroy(gameObject);
                var sfx = FMODUnity.RuntimeManager.CreateInstance(OpenSound);
                if (sfx.isValid()) {
                    sfx.start();
                }
            }
        }
    }

    private bool GetIsOpen() {
        bool isOpen = true;
        isOpen &= GameStateManager.Instance.SaveState.LevelCompletion[Minigame.Bully];
        isOpen &= GameStateManager.Instance.SaveState.LevelCompletion[Minigame.Death];
        isOpen &= GameStateManager.Instance.SaveState.LevelCompletion[Minigame.Flood];
        isOpen &= GameStateManager.Instance.SaveState.LevelCompletion[Minigame.Divorce];
        isOpen &= GameStateManager.Instance.SaveState.LevelCompletion[Minigame.Social];
        return isOpen;
    }
}
