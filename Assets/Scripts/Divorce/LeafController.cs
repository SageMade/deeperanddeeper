using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeafController : MonoBehaviour
{
    public BoxCollider2D RiverExtents;
    public float XSpeed;
    public GameObject ThoughtBubble;
    public Vector2 ItemOffset;
    public bool HasThought {
        get { return ThoughtBubble != null; }
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(XSpeed * Time.deltaTime, 0.0f);
        if (transform.position.x < RiverExtents.bounds.min.x) {
            transform.position = new Vector3(RiverExtents.bounds.max.x, transform.position.y, transform.position.z);
            if (ThoughtBubble != null) {
                Destroy(ThoughtBubble);
                ThoughtBubble = null;
            }
        }
    }

    private void OnDrawGizmosSelected() {
        Gizmos.DrawLine(transform.TransformPoint(ItemOffset + new Vector2(-.2f, -.2f)), transform.TransformPoint(ItemOffset + new Vector2(.2f,  .2f)));
        Gizmos.DrawLine(transform.TransformPoint(ItemOffset + new Vector2(-.2f,  .2f)), transform.TransformPoint(ItemOffset + new Vector2(.2f, -.2f)));
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (ThoughtBubble == null) {
            if (collision.gameObject.CompareTag("Player")) {
                PickupCount pickup = collision.gameObject.GetComponent<PickupCount>();
                if (pickup.pickupTarget != null) {
                    Debug.Log("UwU");
                    Pickup item = pickup.pickupTarget.GetComponent<Pickup>();
                    item.OnDrop?.Invoke();
                    pickup.pickupTarget.GetComponent<Rigidbody2D>().simulated = false;
                    item.isPickedup = false;
                    item.transform.parent = transform;
                    item.transform.localPosition = ItemOffset;
                    ThoughtBubble = item.gameObject;
                    pickup.pickupCounter = 0;
                    pickup.pickupTarget = null;
                }
            }
        }
    }
}
