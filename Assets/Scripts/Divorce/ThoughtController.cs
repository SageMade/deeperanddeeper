using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ThoughtController : MonoBehaviour
{
    public TextMeshProUGUI LabelText;
    public string Text;

    private Coroutine _fadeCoroutine;
    private Color _textCol;

    private void Start() {
        _textCol = LabelText.color;
        _textCol.a = 0.0f;
        LabelText.color = _textCol;
        LabelText.text = Text;
    }

    // Start is called before the first frame update
    public void Pickup() {
        if (_fadeCoroutine != null) { StopCoroutine(_fadeCoroutine); }
        _fadeCoroutine = StartCoroutine(FadeIn());
    }

    public void Drop() {
        if (_fadeCoroutine != null) { StopCoroutine(_fadeCoroutine); }
        _fadeCoroutine = StartCoroutine(FadeOut());
    }

    IEnumerator FadeIn() {
        while (_textCol.a < 1.0f) {
            _textCol.a += Time.deltaTime;
            LabelText.color = _textCol;
            yield return null;
        }
        _fadeCoroutine = null;
    }
    IEnumerator FadeOut() {
        while (_textCol.a > 0.0f) {
            _textCol.a -= Time.deltaTime;
            LabelText.color = _textCol;
            yield return null;
        }
        _fadeCoroutine = null;
    }
}
