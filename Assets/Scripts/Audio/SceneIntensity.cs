using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneIntensity : MonoBehaviour
{
    public float Intensity = 0.0f;

    // Start is called before the first frame update
    void Start() {
        AudioController.Instance.SetIntensity(Intensity);
    }
}
