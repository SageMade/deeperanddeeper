using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    FMOD.Studio.EventInstance SFXVolumeTestEvent;

    FMOD.Studio.Bus Music;
    FMOD.Studio.Bus SFX;
    FMOD.Studio.Bus Master;
    FMOD.Studio.Bus Ambient;

    public FMODUnity.StudioEventEmitter MusicEmitter;

    private float _musicVolume = 0.5f;
    public float MusicVolume {
        get { return _musicVolume; }
        set { SetMusicVolumeLevel(value); }
    }


    private float _sfxVolume = 0.5f;
    public float SFXVolume {
        get { return _sfxVolume; }
        set { SetSFXVolumeLevel(value); }
    }
    private float _masterVolume = 0.5f;
    public float MasterVolume {
        get { return _masterVolume; }
        set { SetSFXVolumeLevel(value); }
    }

    protected bool _isSettingsDirty = true;

    private static AudioController _instance;
    public static AudioController Instance {
        get { return _instance; }
    }

    private float _intensity = 0.0f;
    private Coroutine _intensityCR;

    void Awake()
    {
        // Audio controller singleton
        if (_instance == null) {
            _instance = this;
            DontDestroyOnLoad(this);
        } else {
            Destroy(this);
            return;
        }

        Music = FMODUnity.RuntimeManager.GetBus("bus:/Music");
        SFX = FMODUnity.RuntimeManager.GetBus("Bus:/SFX");
        Master = FMODUnity.RuntimeManager.GetBus("bus:/Master");
        SFXVolumeTestEvent = FMODUnity.RuntimeManager.CreateInstance("event:/SFX/Test Sounds/SFX volume adjustment");

        MasterVolume = PlayerPrefs.GetFloat("audio.volume.master", MasterVolume);
        MusicVolume = PlayerPrefs.GetFloat("audio.volume.music", MusicVolume);
        SFXVolume = PlayerPrefs.GetFloat("audio.volume.sfx", SFXVolume);
    }

    public void FireSound(string soundEvent) {
        try {
            var instance = FMODUnity.RuntimeManager.CreateInstance(soundEvent);
            if (instance.isValid()) {
                instance.start();
            }
        } catch (Exception e) { }
    }

    public void FireSound(string soundEvent, Vector2 pos) {
        try {
            var instance = FMODUnity.RuntimeManager.CreateInstance(soundEvent);
            if (instance.isValid()) {
                FMOD.ATTRIBUTES_3D attribs = new FMOD.ATTRIBUTES_3D();
                attribs.position.x = pos.x;
                attribs.position.y = pos.y;
                attribs.forward.z = -1;
                attribs.up.y = 1;
                instance.set3DAttributes(attribs);
                instance.start();
            }
        } catch (Exception e) { }
    }

    public void SaveSettings() {
        PlayerPrefs.SetFloat("audio.volume.music",   MusicVolume);
        PlayerPrefs.SetFloat("audio.volume.sfx",     SFXVolume);
        PlayerPrefs.SetFloat("audio.volume.master",  MasterVolume);
        PlayerPrefs.Save();
    }
 
    // Update is called once per frame
    void Update()
    {
        if (_isSettingsDirty) {
            SFX.setVolume(SFXVolume);
            Master.setVolume(MasterVolume);
            _isSettingsDirty = false;
        }
    }

    public void SetIntensity(float intensity, float fadeTime = 1.0f) {
        _intensity = intensity;
        MusicEmitter.EventInstance.setParameterByName("Intensity", _intensity);
        Debug.Log("Setting music intensity"); 
        //if (_intensityCR != null) { StopCoroutine(_intensityCR); }
        //_intensityCR = StartCoroutine(ChangeIntensity(intensity, fadeTime));
    }

    private IEnumerator ChangeIntensity(float newValue, float fadeTime) {
        float vel = 0.0f;
        while (!Mathf.Approximately(_intensity, newValue)) {
            _intensity = Mathf.SmoothDamp(_intensity, newValue, ref vel, fadeTime, 1.0f, Time.unscaledDeltaTime);
            MusicEmitter.EventInstance.setParameterByName("Intensity", _intensity);
            yield return null;
        }
        _intensityCR = null;
    }

    public void SetMasterVolumeLevel(float newMasterVolume)
    {
        _masterVolume = newMasterVolume;
        Master.setVolume(_masterVolume);
        _isSettingsDirty = true;
    }

    public void FadeMusic(float time) {
        StartCoroutine(_FadeMusic(time));
    }

    private IEnumerator _FadeMusic(float time) {
        float v = 0;
        while (!Mathf.Approximately(_musicVolume, 0.0f)) {
            _musicVolume = Mathf.SmoothDamp(_musicVolume, 0.0f, ref v, time, 1.0f, Time.unscaledDeltaTime);
            Music.setVolume(_musicVolume);
            yield return null;
        }
    }

    public void SetMusicVolumeLevel(float newMusicVolume)
    {
        _musicVolume = newMusicVolume;
        Music.setVolume(_musicVolume);
        _isSettingsDirty = true;
    }
    public void SetSFXVolumeLevel(float newSFXVolume)
    {
        _sfxVolume = newSFXVolume;
        SFX.setVolume(_sfxVolume);
        _isSettingsDirty = true;

        FMOD.Studio.PLAYBACK_STATE PbState;
        SFXVolumeTestEvent.getPlaybackState(out PbState);
        if (PbState != FMOD.Studio.PLAYBACK_STATE.PLAYING)
        {
            SFXVolumeTestEvent.start();
        }
    }
}
