using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DivorceGameManager : MonoBehaviour
{
    [Header("Level Specific")]
    [SerializeField] private TransitionTextAsset OnDeathText;
    public TransitionTextAsset CompletionText;
   // private ThoughtController[] orbs;
    Divorce divorce;

    // Start is called before the first frame update
    void Start()
    {
        GameStateManager.Instance.OnPlayerHealthChanged.AddListener(OnPlayerHealthChanged);
        divorce = GameObject.Find("Orbs").GetComponent<Divorce>();
    }

    private void Update()
    {
        if (divorce.levelComplete)
        {
            StartCoroutine(FinishLevel());
        }
    }

    private void OnDestroy()
    {
        GameStateManager.Instance.OnPlayerHealthChanged.RemoveListener(OnPlayerHealthChanged);
    }

    private void OnPlayerHealthChanged(float health, float max)
    {
        if (health <= 0.0f)
        {
            GameStateManager.Instance.Player.enabled = false;
            GameStateManager.Instance.TargetScene = "Scenes/Divorce";
            GameStateManager.Instance.TransitionText = OnDeathText;
            SceneTransitionManager.Instance.FadeToScene("Scenes/TransitionScene");
        }
    }

    IEnumerator FinishLevel()
    {
        GameStateManager.Instance.Player.isRunning = false;
        GameStateManager.Instance.Player.rb.simulated = false;
        GameStateManager.Instance.Player.enabled = false;

        GameObject[] orbs;
        orbs = GameObject.FindGameObjectsWithTag("Orb");
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;

        foreach (GameObject i in orbs)
        {
            Vector3 diff = i.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if(curDistance < distance)
            {
                closest = i;
                distance = curDistance;
            }
        }

        FindObjectOfType<FollowPlayer>().player = closest.transform;
        FindObjectOfType<FollowPlayer>().altFocus = null;

        float time = 0.0f;
        while (time < 5.0f)
        {
            time += Time.deltaTime;

            yield return null;
        }

        GameStateManager.Instance.LevelCompletion[Minigame.Divorce] = true;
        GameStateManager.Instance.TargetScene = "Scenes/HubWorld";
        GameStateManager.Instance.TransitionText = CompletionText;
        SceneTransitionManager.Instance.FadeToScene("Scenes/TransitionScene");

    }
}
