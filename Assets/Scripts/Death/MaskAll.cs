using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Tilemaps;

public class MaskAll : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        SpriteRenderer[] renderers = gameObject.GetComponentsInChildren<SpriteRenderer>();
        foreach(SpriteRenderer renderer in renderers) {
            renderer.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
        }
        TilemapRenderer[] tilemaps = gameObject.GetComponentsInChildren<TilemapRenderer>();
        foreach (TilemapRenderer renderer in tilemaps) {
            renderer.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
        }
    }
}
