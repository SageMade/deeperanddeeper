using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;
using System;

public class DeathGameManager : MonoBehaviour
{
    public TransitionTextAsset CompletionText;
    public TransitionTextAsset DeathText;
    public Transform Mask;
    public GameObject GoreBurst;
    public List<GameObject> Collectables;
    public float StartScale = 2.0f;
    public float MaxScale   = 10.0f;

    private int _numCollectables;
    private PickupCount _pickup;
    private Vector3 _minScale;
    private Vector3 _maxScale;

    private Coroutine _cr;

    // Start is called before the first frame update
    void Start() {
        GameStateManager.Instance.OnPlayerHealthChanged.AddListener(OnPlayerHealthChanged);
        if (Collectables.Count == 0) {
            Collectables = FindObjectsOfType<Pickup>().Select(x => x.gameObject).ToList();
        }
        _numCollectables = Collectables.Count;
        _pickup = FindObjectOfType<PickupCount>();
        _minScale = new Vector3(StartScale, StartScale, 1.0f);
        _maxScale = new Vector3(MaxScale, MaxScale, 1.0f);
        Mask.localScale = _minScale;

        for (int ix = 0; ix < Collectables.Count; ix++) {
            Pickup item = Collectables[ix].GetComponent<Pickup>();
            if (item) {
                item.OnPickup.AddListener(() => {
                    OnItemCollected(item);
                });
            }
        }
    }

    private void OnDestroy() {
        GameStateManager.Instance.OnPlayerHealthChanged.RemoveListener(OnPlayerHealthChanged);
    }

    private void OnPlayerHealthChanged(float health, float maxHealth) {
        if (health <= 0.0f) {
            var player = GameStateManager.Instance.Player;
            player.GetComponentInChildren<SpriteRenderer>().color = new Color(0, 0, 0, 0);
            GameObject gore = Instantiate(GoreBurst);
            gore.transform.position = player.transform.position;
            StartCoroutine(Die());
        }
    }

    private void OnItemCollected(Pickup obj) {
        _pickup.pickupTarget = null;
        _pickup.pickupCounter = 0;
        obj.OnDrop?.Invoke();
        float currentT = 1.0f - (Collectables.Count / (float)_numCollectables);
        Collectables.Remove(obj.gameObject);
        GameObject.Destroy(obj.gameObject);
        float newT = 1.0f - (Collectables.Count / (float)_numCollectables);

        if (Collectables.Count == 0) {
            if (_cr != null) { StopCoroutine(_cr); }
            _cr = StartCoroutine(FinishLevel());
        } else {
            if (_cr != null) { StopCoroutine(_cr); }
            _cr = StartCoroutine(ExpandView(Vector3.Lerp(_minScale, _maxScale, newT)));
        }
    }


    IEnumerator ExpandView(Vector3 newScale) {
        Vector3 vel = Vector3.zero;
        while (Vector3.Distance(Mask.localScale, newScale) > float.Epsilon) {
            Mask.localScale = Vector3.SmoothDamp(Mask.localScale, newScale, ref vel, 1.0f);
            yield return null;
        }
        _cr = null;
    }

    IEnumerator FinishLevel() {
        GameStateManager.Instance.Player.isRunning = false;
        GameStateManager.Instance.Player.rb.simulated = false;
        GameStateManager.Instance.Player.enabled = false;
        FollowPlayer cam = FindObjectOfType<FollowPlayer>();
        float time = 0.0f;
        while (time < 3.0f) {
            time += Time.deltaTime;
            Mask.localScale += new Vector3(Time.deltaTime, Time.deltaTime) * 10.0f;
            cam.minCamScale += Time.deltaTime * 5.0f;
            yield return null;
        }

        GameStateManager.Instance.LevelCompletion[Minigame.Death] = true;
        GameStateManager.Instance.TargetScene = "Scenes/HubWorld";
        GameStateManager.Instance.TransitionText = CompletionText;
        SceneTransitionManager.Instance.FadeToScene("Scenes/TransitionScene");
    }

    IEnumerator Die() {
        FindObjectOfType<FollowPlayer>().enabled = false;
        yield return new WaitForSeconds(1.0f);

        GameStateManager.Instance.TargetScene = SceneManager.GetActiveScene().name;
        GameStateManager.Instance.TransitionText = DeathText;
        SceneTransitionManager.Instance.FadeToScene("Scenes/TransitionScene");
    }
}
