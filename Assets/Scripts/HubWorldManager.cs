using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HubWorldManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (GameStateManager.Instance.HubTeleporterPos.HasValue) {
            GameStateManager.Instance.Player.transform.position = GameStateManager.Instance.HubTeleporterPos.Value;
        }
    }
}
