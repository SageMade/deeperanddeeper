using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Portal : MonoBehaviour
{
    [SceneSelector]
    public string TargetScene;
    public Minigame Game;
    public TransitionTextAsset TransitionText;
    public Transform TeleportReturnPos;

    public SpriteRenderer ChainRenderer;
    public Sprite Chain;
    public Sprite ChainBroken;

    [FMODUnity.EventRef]
    public string EnterSound;

    private void Awake() {
        if (GameStateManager.Instance.LevelCompletion[Game]) {
            Destroy(this);
            ChainRenderer.sprite = ChainBroken;
        } else {
            ChainRenderer.sprite = Chain;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.CompareTag("Player")) {
            GameStateManager.Instance.TargetScene = TargetScene;
            GameStateManager.Instance.HubTeleporterPos = TeleportReturnPos == null ? transform.position : TeleportReturnPos.position;
            GameStateManager.Instance.TransitionText = TransitionText;
            SceneTransitionManager.Instance.FadeToScene("Scenes/TransitionScene");

            AudioController.Instance.FireSound(EnterSound);
        }
    }
}
