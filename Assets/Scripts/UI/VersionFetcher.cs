using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TMPro.TextMeshProUGUI))]
public class VersionFetcher : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        TMPro.TextMeshProUGUI text = GetComponent<TMPro.TextMeshProUGUI>();
        text.text = Application.version;
    }
}
