using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PauseMenuManager : MonoBehaviour
{
    public GameObject Panel;
    public GameObject ButtonsParent;
    public EventSystem Events;

    public bool Visible {
        get { return Panel.active; }
        set {
            if (Visible) {
                ResumeGame();
            } else {
                Show();
            }
        }
    }

    public void Update() {
        if (Input.GetButtonDown("Start")) {
            Visible = !Visible;
        }

        if (Events.currentSelectedGameObject == null || !Events.currentSelectedGameObject.activeInHierarchy || !Events.currentSelectedGameObject.CompareTag("MenuButton")) {
            for (int ix = 0; ix < ButtonsParent.transform.childCount; ix++) {
                Button b = ButtonsParent.transform.GetChild(ix).GetComponent<Button>();
                if (b.interactable) {
                    b.Select();
                    break;
                }
            }
        }
    }

    public void Show() {
        Time.timeScale = 0.0f;
        Panel.active = true;
    }

    public void ResumeGame() {
        Time.timeScale = 1.0f;
        Panel.active = false;
    }

    public void QuitToMenu() {
        SceneTransitionManager.Instance.FadeToScene("Scenes/MainMenu");
    }

    public void QuitGame() {
        Application.Quit();
    }
}
