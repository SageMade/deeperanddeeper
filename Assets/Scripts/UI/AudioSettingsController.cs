using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSettingsController : MonoBehaviour {
    public UnityEngine.UI.Slider MasterSlider;
    public UnityEngine.UI.Slider MusicSlider;
    public UnityEngine.UI.Slider SFXSlider;

    public enum Slider {
        Master,
        Music,
        SFX
    }

    void Start() {
        _LoadSettings();
    }

    protected void _LoadSettings() {
        MasterSlider?.SetValueWithoutNotify(AudioController.Instance.MasterVolume);
        MusicSlider?.SetValueWithoutNotify(AudioController.Instance.MusicVolume);
        SFXSlider?.SetValueWithoutNotify(AudioController.Instance.SFXVolume);
    }

    public void MasterVolumeChanged(float value) {
        AudioController.Instance.MasterVolume = value;
    }
    public void MusicVolumeChanged(float value) {
        AudioController.Instance.MusicVolume = value;
    }
    public void SFXVolumeChanged(float value) {
        AudioController.Instance.SFXVolume = value;
    }

    public void Apply() {
        AudioController.Instance.SaveSettings();
    }

    public void Revert() {
        _LoadSettings();
    }
}
