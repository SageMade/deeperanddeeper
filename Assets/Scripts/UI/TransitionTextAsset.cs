using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TransitionText", menuName = "Custom/Scene Transition Text")]
public class TransitionTextAsset : ScriptableObject {
    public string[] Lines;
}
