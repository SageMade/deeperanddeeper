using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextDistanceFade : MonoBehaviour
{
    public TextMeshProUGUI Text;
    public float MaxDistance = 4.0f;
    private Transform _player;
    private Color _color;
    private Coroutine _fadeCoroutine;
    private bool _inRadius;

    private void Start() {
        _player = GameStateManager.Instance.Player.transform;
        if (Text == null) { Text = GetComponentInChildren<TextMeshProUGUI>(); }
        _color = Text.color;
        _color.a = 0.0f;
        Text.color = _color;
    }

    private void Update() {
        if (Vector3.Distance(transform.position, _player.position) < MaxDistance) {
            if (!_inRadius) {
                _inRadius = true;
                if (_fadeCoroutine != null) { StopCoroutine(_fadeCoroutine); }
                _fadeCoroutine = StartCoroutine(FadeIn());
            }
        } else {
            if (_inRadius) {
                _inRadius = false;
                if (_fadeCoroutine != null) { StopCoroutine(_fadeCoroutine); }
                _fadeCoroutine = StartCoroutine(FadeOut());
            }
        }
    }

    IEnumerator FadeIn() {
        while (_color.a < 1.0f) {
            _color.a += Time.deltaTime;
            Text.color = _color;
            yield return null;
        }
        _fadeCoroutine = null;
    }
    IEnumerator FadeOut() {
        while (_color.a > 0.0f) {
            _color.a -= Time.deltaTime;
            Text.color = _color;
            yield return null;
        }
        _fadeCoroutine = null;
    }
}
