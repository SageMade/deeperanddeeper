using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class Healthbar : MonoBehaviour
{
    public Image HandleImage;
    public RectTransform HealthSlider;
    public TMPro.TextMeshProUGUI Text;
    public Color LowHealth = Color.red;
    public Color HighHealth = Color.green;
    
    [System.Serializable]
    public struct HealthMoods {
        public float Cutoff;
        public string Text;
    }
    [SerializeField]
    public HealthMoods[] Moods;

    // Start is called before the first frame update
    void Start() {
        GameStateManager.Instance.OnPlayerHealthChanged.AddListener(OnHealthChanged);
        Moods = Moods.OrderBy(x => x.Cutoff).ToArray();
        if (Moods.Length == 0) {
            Array.Resize(ref Moods, 1);
            Moods[0].Cutoff = 0.0f;
            Moods[0].Text = "<missing>";
        }
        OnHealthChanged(GameStateManager.Instance.PlayerHealth, GameStateManager.Instance.MaxPlayerHealth);
    }

    private void OnDestroy() {
        GameStateManager.Instance.OnPlayerHealthChanged.RemoveListener(OnHealthChanged);
    }

    private void OnHealthChanged(float health, float max) {
        Debug.Log("Updating Health");
        string text = "<missing>";
        for (int ix = Moods.Length - 1; ix >= 0; ix --) {
            if (health > Moods[ix].Cutoff) {
                text = Moods[ix].Text;
                break;
            }
        }
        Text?.SetText(text);
        HandleImage.color = Color.Lerp(LowHealth, HighHealth, (health / max));
        HandleImage.rectTransform.sizeDelta = new Vector2((health / max) * HealthSlider.sizeDelta.x, HandleImage.rectTransform.sizeDelta.y);
    }

    // Update is called once per frame
    void Update() {
        
    }
}
