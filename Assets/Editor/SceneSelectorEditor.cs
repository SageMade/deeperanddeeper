using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// Provides editor support for dragging scene assets onto a LevelSelector component
/// </summary>
[CustomPropertyDrawer(typeof(SceneSelectorAttribute))]
public class SceneSelectorEditor : PropertyDrawer {
    private string _ActualPath;

    public override void OnGUI(Rect pos, SerializedProperty serializedProp, GUIContent label) {
        string path = serializedProp.stringValue; 
        SceneAsset oldScene = null;
        // If we don't already know the asset path
        if (_ActualPath == null) {
            // If the target actually has a value set, we need to look for that scene
            if (!string.IsNullOrWhiteSpace(path)) {
                // Search for SceneAssets that match the name stored in the selector
                string[] guids = AssetDatabase.FindAssets(string.Format("{0} t:SceneAsset", path));
                // As long as we found at least one asset that matches
                if (guids.Length > 0) {
                    // Load the scene asset using that asset's path (we take the first one)
                    oldScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(AssetDatabase.GUIDToAssetPath(guids[0]));
                }
            }
        }
        // If we already know the path, simply load the asset
        else {
            oldScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(_ActualPath);
        }

        // Get our latest serialized properties
        //serializedProp.Update();

        // Start listening for changes to the editor
        EditorGUI.BeginChangeCheck();
        // Draw the drop target for scenes
        var newScene = EditorGUI.ObjectField(pos, label, oldScene, typeof(SceneAsset), false) as SceneAsset;

        // If one of our properties has been modified
        if (EditorGUI.EndChangeCheck()) {
            // Get and store the full path of the asset
            string newPath = AssetDatabase.GetAssetPath(newScene);
            _ActualPath = newPath;
            // Determine the scene name from the path filename
            newPath = System.IO.Path.GetFileNameWithoutExtension(newPath);

            // Get the TargetScene property from the serialized context, and update it
            serializedProp.stringValue = newPath;
        }
    }
}
